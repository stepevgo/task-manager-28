package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.IRepository;
import ru.t1.stepanishchev.tm.api.service.IService;
import ru.t1.stepanishchev.tm.exception.entity.ModelNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.IndexIncorrectException;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    @Nullable
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    @Nullable
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(model);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    @NotNull
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    @Nullable
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    @Nullable
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Override
    @Nullable
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}