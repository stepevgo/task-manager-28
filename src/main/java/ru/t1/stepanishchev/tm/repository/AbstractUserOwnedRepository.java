package ru.t1.stepanishchev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.exception.entity.ModelNotFoundException;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    @Nullable
    public M add(@Nullable final String userId, @Nullable final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        @Nullable final List<M> models = findAll(userId);
        models.sort(comparator);
        return models;
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        findOneById(userId, id);
        return true;
    }

    @Override
    @Nullable
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId.equals(model.getUserId())) {
            models.remove(model);
        }
        return model;
    }

    @Override
    @Nullable
    public M removeOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return removeOne(userId, model);
    }

    @Override
    @Nullable
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        @Nullable final M model = findOneById(userId, id);
        return removeOne(userId, model);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}